# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
Mentee.delete_all
Grant.delete_all
UniversityService.delete_all
OutreachActivity.delete_all
Publication.delete_all
ProfessionalService.delete_all
Course.delete_all
Report.delete_all
Person.delete_all
Department.delete_all
College.delete_all



business = College.create(name:'Anderson School of Management')
arts = College.create(name:'College of Arts & Sciences')
edu = College.create(name:'College of Education')
fine = College.create(name:'College of Fine Arts')
grad = College.create(name:'Graduate Studies')
honors = College.create(name:'Honors College')
nurse = College.create(name:'College of Nursing')
pharm = College.create(name:'College of Pharmacy')
pop = College.create(name:'College of Population Health')
library = College.create(name:'College of University Libraries & Learning Sciences')
arch = College.create(name:'School of Architecture and Planning')
law = College.create(name:'School of Law')
engineering = College.create(name:'School of Engineering')
med = College.create(name:'School of Medicine')
mpa = College.create(name:'School of Public Administration')
ucol = College.create(name:'University College')

arch.departments.build(name: 'Architecture')
arch.departments.build(name: 'Community & Regional Planning')
arch.departments.build(name: 'Historic Preservation & Regionalism')
arch.departments.build(name: 'Urban & Regional Design')
arch.departments.build(name: 'Landscape Architecture')
arch.departments.build(name: 'Deans Office')
arch.save

arts.departments.build(name: 'American Studies')
arts.departments.build(name: 'Anthropology')
arts.departments.build(name: 'Biology')
arts.departments.build(name: 'Chemistry and Chemical Biology')
arts.departments.build(name: 'Chicana and Chicano Studies')
arts.departments.build(name: 'Communication & Journalism')
arts.departments.build(name: 'Earth & Planetary Sciences')
arts.departments.build(name: 'Economics')
arts.departments.build(name: 'English')
arts.departments.build(name: 'Foreign Languages & Literature')
arts.departments.build(name: 'Geography')
arts.departments.build(name: 'History')
arts.departments.build(name: 'Linguistics')
arts.departments.build(name: 'Mathematics & Statistics')
arts.departments.build(name: 'Philosophy')
arts.departments.build(name: 'Physics & Astronomy')
arts.departments.build(name: 'Political Science')
arts.departments.build(name: 'Psychology')
arts.departments.build(name: 'Sociology')
arts.departments.build(name: 'Spanish & Portuguese')
arts.departments.build(name: 'Speech & Hearing Sciences')
arts.departments.build(name: 'Deans Office')
arts.save


business.departments.build(name: 'Accounting')
business.departments.build(name: 'Finance, International, Technology and Entrepeneurship (FITE)')
business.departments.build(name: 'Marketing, Information Systems, Information Assurance, & Operations Management Faculty (MIDS)')
business.departments.build(name: 'Department of Organizational Studies (DOS)')
business.departments.build(name: 'Deans Office')
business.save

edu.departments.build(name: 'Department of Special Education (SPCD)')
edu.departments.build(name: 'Department of Health, Exercise, and Sports Sciences (HESS)')
edu.departments.build(name: 'Department of Individual, Family, and Community Education (IFCE)')
edu.departments.build(name: 'Department of Language, Literacy, and Sociocultural Studies (LLSS)')
edu.departments.build(name: 'Department of Teacher Education, Educational Leadership & Policy (TEELP)')
edu.departments.build(name: 'Deans Office')
edu.save

fine.departments.build(name: 'Art')
fine.departments.build(name: 'Music')
fine.departments.build(name: 'Theatre & Dance')
fine.departments.build(name: 'Cinematic Arts')
fine.departments.build(name: 'Deans Office')
fine.save

grad.departments.build(name: 'Deans Office')
grad.save

honors.departments.build(name: 'Deans Office')
honors.save


engineering.departments.build(name: 'Civil Engineering')
engineering.departments.build(name: 'Chemical and Biological Engineering')
engineering.departments.build(name: 'Computer Science')
ece = engineering.departments.build(name: 'Electrical and Computer Engineering')
engineering.departments.build(name: 'Mechanical Engineering')
engineering.departments.build(name: 'Nuclear Engineering')
eng_dean = engineering.departments.build(name: 'Deans Office')
engineering.save

law.departments.build(name: 'Clinical Law')
law.departments.build(name: 'Indian Law')
law.departments.build(name: 'Institute of Public Law')
law.departments.build(name: 'Natural Resources Journal')
law.departments.build(name: 'New Mexico Law Review')
law.departments.build(name: 'Southwest Indian Law Clinic')
law.departments.build(name: 'Utton Transboundary Resources Center')
law.departments.build(name: 'Deans Office')
law.save

library.departments.build(name: 'University Libraries')
library.departments.build(name: 'Organization, Information & Learning Sciences')
library.departments.build(name: 'Deans Office')
library.save

med.departments.build(name: 'Anesthesiology and Critical Care Medicine')
med.departments.build(name: 'Biochemistry and Molecular Biology')
med.departments.build(name: 'Cell Biology and Physiology')
med.departments.build(name: 'Dental Medicine')
med.departments.build(name: 'Dermatology')
med.departments.build(name: 'Emergency Medicine')
med.departments.build(name: 'Family and Community Medicine')
med.departments.build(name: 'Internal Medicine')
med.departments.build(name: 'Molecular Genetics and Microbiology')
med.departments.build(name: 'Neurology')
med.departments.build(name: 'Obstetrics and Gynecology')
med.departments.build(name: 'Orthopaedics and Rehabilitation')
med.departments.build(name: 'Pathology')
med.departments.build(name: 'Pediatrics')
med.departments.build(name: 'Psychiatry and Behavioral Sciences')
med.departments.build(name: 'Radiology')
med.departments.build(name: 'Surgery')
med.departments.build(name: 'Deans Office')
med.save

mpa.departments.build(name: 'Public Administration')
mpa.departments.build(name: 'Health Administration')
mpa.departments.build(name: 'Deans Office')
mpa.save

nurse.departments.build(name: 'Deans Office')
nurse.save

pharm.departments.build(name: 'Pharmacy Practice and Administrative Sciences')
pharm.departments.build(name: 'Pharmaceutical Sciences')
pharm.departments.build(name: 'Deans Office')
pharm.save

pop.departments.build(name: 'Deans Office')
pop.save

ucol.departments.build(name: 'Liberal Arts and Integrative Studies')
ucol.departments.build(name: 'Engaged Learning and Research')
ucol_dean = ucol.departments.build(name: 'Deans Office')
ucol.save

admin = Person.create(first_name: 'Admin',
                      last_name: 'Admin',
                      net_id: 'admin',
                      banner_id: '000000000',
                      college: ucol,
                      department: ucol_dean)

admin.add_role(:global_admin)
admin.save

greg = Person.create( first_name: 'Gregory',
                      last_name: 'Heileman',
                      net_id: 'heileman',
                      banner_id: '987654321',
                      college: engineering,
                      department: ece)
greg.add_role(:user, ece)
greg.save
year2015 = '2015-1-1'.to_date
year2016 = '2016-1-1'.to_date
report_open_time2015 = DateTime.parse("2015-02-01 04:00")
report_open_time2016 = DateTime.parse("2016-02-01 04:00")
report_close_time2015 = DateTime.parse("2015-05-15 23:59")
report_close_time2016 = DateTime.parse("2016-05-15 23:59")

greg_report2015 = greg.reports.build(report_year: year2015,
                                  open_time: report_open_time2015,
                                  close_time: report_close_time2015,
                                  rank: 'professor',
                                  department: ece.name,
                                  self_evaluation_text: 'This year was an excellent year for my group. I mentored over 200 graduate students, including 8 PhD students. Through the hard work of my students and staff, we managed to make ground breaking progress on silicon enchanced microchip processors, increasing power efficiency by 15%. Additionally through my efforts, I managed to secure over $15 million in funding for the University and the School of Engineering.',
                                  future_plans_text: 'I would like to continue my research into silicon enhanced microprocessors. I believe that by ensuring cleanliness during the manufacturing process, defects can be reduced even further, leading to better performance. Additionally I would like to begin a project with the School of Medicine that would invovle studying the potential of an AI entity to perform medical diagnosis.')
greg_report2015.save
greg_report2015.publications.build(publication_type: 'Journal Article',
                                   body: 'Heileman, G. (2015). Silicon enhanced microchip processor enhancements using novel methods. Journal of Electrical Engineering May 2015. 145-160.',
                                   url: 'https://www.ecejournal.org/2015/May.pdf')
greg_report2015.publications.build(publication_type: 'Textbook',
                                   body: 'Heileman, G. & Hunt, P. (2015). Modern Electronics. UNM Publishing',
                                   url: 'https://www.amazon.com/modern_electronics-heilemen.asp')
greg_report2015.save
greg_report2015.courses.build(course_number: 'ECE 355',
                              course_title: 'Introduction to Electrical Processes',
                              enrollment: 33,
                              year: year2015,
                              semester: 'spring',
                              evaluation_results: 'Excellent feedback from class. Most students felt the learning process went well.',
                              taught: TRUE)
greg_report2015.courses.build(course_number: 'ECE 410',
                              course_title: 'Advanced Computer Mathematical Theory',
                              enrollment: 10,
                              year: year2015,
                              semester: 'spring',
                              evaluation_results: 'Most students had little feedback. One comment that requested more use of media during lecutures. Overall performance was satisfactory.',
                              taught: TRUE)
greg_report2015.courses.build(course_number: 'ECE 310',
                              course_title: 'Number Theory and Computer Science',
                              enrollment: 45,
                              year: year2015,
                              semester: 'fall',
                              evaluation_results: 'Large variety of responses. Some students felt the exams did not fully reflect the homework given, however other students said they had no issues with difficulty of exams or consistency. Overall the performance of the class was excellent. The feedback was similar to previous years taught.',
                              taught: TRUE)
greg_report2015.courses.build(course_number: 'ECE 410',
                              course_title: 'Artificial Intelligence in the Modern Computing World',
                              enrollment: 14,
                              year: year2015,
                              semester: 'fall',
                              evaluation_results: '100% of students had positive feedback, with several indicating they would like to learn more about the field of artificial intelligence. I am considering creating a follow-on class to delve deeper in the field.',
                              taught: TRUE)
greg_report2015.courses.build(course_number: 'ECE 515',
                              course_title: 'Advanced Microsystems and Processes',
                              enrollment: 12,
                              year: year2015,
                              semester: 'fall',
                              evaluation_results: 'Supervised my teaching assistants in the lectures and administration of the course. Overall they could use improvement in both lesson plan building and the construction of effective written examinations.',
                              taught: FALSE)
greg_report2015.save
greg_report2015.professional_services.build(title: 'IEEE 2015 Conference Organizer',
                                            description: 'Provided meeting and conference management services for IEEE professional engineering association. Partnered with volunteers to ensure smooth conference execution and completion.')
greg_report2015.professional_services.build(title: 'Vice President, Institute of Electrical and Electronics Engineers Computer Society Albuquerque Chapter',
                                            description: 'Vice President of local chapter of IEEE Computer society. Led over 20 members to contribute to 3 technical conferences nationwide, including the Rock Star event held in July in Seattle, WA. Additionally the local chapter performed 15 talks at local high schools and elementaries to encourage young people to consider Computers as a career choice.')
greg_report2015.save
greg_report2015.university_services.build(position: 'Faculty Advisor',
                                          description: 'Provided professional advice for successful meetings of UNM NEST (Network Exploitation Security Team). Provided additional guidance for fund raising efforts and organizational challenges.')
greg_report2015.university_services.build(position: 'Associate Contributer',
                                          description: 'Provided research support to the Center for High Technology Materials. Support included helping the nanoscale lithography and nanofacbrication lab continue their experiments after a castostrophic failure of a key component in the equipment.')
greg_report2015.save
greg_report2015.outreach_activities.build(name: 'East San Jose Elementary School May 15',
                                          description: 'Helped 5th graders at the Easwt San Jose Elementary School in Albuquerque, NM to learn about light and LEDs and make a simple art project with LEDs.')
greg_report2015.outreach_activities.build(name: 'National Museum of Nuclear Science and History - Feb 13',
                                          description: 'Talks/demos to K-12 students about STEM career experiences na dstudies at Engineering Week at the National Museum of Nuclear Science and History, Albuquerque NM.')
greg_report2015.save
greg_report2015.grants.build(title: 'Electronics in Extreme Electromagnetic Environments',
                             status: 1,
                             duration: '2 years',
                             total_funding: 30000000,
                             unm_portion: 6000000,
                             soe_portion: 6000000,
                             granscol: 'School of Engineering')
greg_report2015.save
may2017 = '2017-5-1'.to_date
greg_report2015.mentees.build(mentee_type: 'phd_student',
                              first_name: 'Chris',
                              middle_name: 'Pine',
                              last_name: 'Rock',
                              banner_id: '9998939899',
                              study_stage: 'Final Year',
                              expected_graduation: may2017,
                              support_source: 'GPSA Grant (UNM)',
                              placement: 'unknown')
greg_report2015.mentees.build(mentee_type: 'masters_student',
                              first_name: 'Richie',
                              middle_name: 'Yu',
                              last_name: 'Smith',
                              banner_id: '222233300',
                              study_stage: '35% complete',
                              expected_graduation: may2017,
                              support_source: 'Self Funded',
                              placement: 'unknown')
greg_report2015.save

greg_report2016 = greg.reports.build(report_year: year2016,
                                     open_time: report_open_time2016,
                                     close_time: report_close_time2016,
                                     rank: 'professor',
                                     department: ece.name,
                                     self_evaluation_text: 'An even better year than 2015, I oversaw the completion of several major projects that directly led to contributing to the field of microelectronics. I mentored over 30 graduate students, and saw 6 M.S. graduates and 3 P.H.D students gradute. Through my dedicated leadership and commitment to procuring further sources of funding, I established a continuing grant with the Department of Energy that should yield UNM over $50 mililion over the next decade.',
                                     future_plans_text: 'Due to the laborious schedule I maintained with my teaching and research projects, I am hoping to take a 6 month sabbatical next spring, followed by a gradual reintroduction to the acacademic world by teaching a few classes next fall.')
greg_report2016.publications.build(publication_type: 'Analytical Essay',
                                   body: 'Heileman, G. (2016). An analysis of a novel method to produce microchips in a vacuum environment near absolute zero temperature.',
                                   url: 'https://www.ecereview.org/2016/gheileman1.pdf')
greg_report2016.publications.build(publication_type: 'Journal Article',
                                   body: 'Heileman, G. & Majheet Satuk. (2016) ECE Journal.  Hybridized Soft Computing Approaches Based Data Mining Techniques for Medical Uses 78-85',
                                   url: 'http://www.ecejournal.org/2016/may/000324/index.html')
greg_report2016.save
greg_report2016.courses.build(course_number: 'ECE 322',
                              course_title: 'Electrical Theory and Computing',
                              year: year2016,
                              semester: 'spring',
                              enrollment: 45,
                              evaluation_results: 'A mixed feeling from the students, with many complaining that the fire drill during the midterm exam was unfair, despite the University providing a makeup exam. Overall the students performed well, and the feedback for my teaching style was positive.',
                              taught: TRUE)
greg_report2016.courses.build(course_number: 'ECE 512',
                              course_title: 'Advanced Operating System Design',
                              semester: 'spring',
                              year: year2016,
                              enrollment: 6,
                              evaluation_results: 'No major comments from students. All seemed to enjoy and learn from the class.',
                              taught: TRUE)
greg_report2016.courses.build(course_number: 'ECE 401',
                              course_title: 'Computer Design and Theory',
                              semester: 'fall',
                              year: year2016,
                              enrollment: 14,
                              evaluation_results: 'A highly motivated class with excellent feedback at the end. Major comments included complaints about the state of the facilities in the main ECE building. Also some students noted that the homework did not always match the exams given.',
                              taught: TRUE)
greg_report2016.courses.build(course_number: 'ECE 509',
                              course_title: 'Big Data and Operating Systems',
                              semester: 'fall',
                              year: year2016,
                              enrollment: 9,
                              evaluation_results: 'Nothing of note to report. Most students had nothing to report at the end. One student did mention wishing to learn more about online web service providers.',
                              taught: TRUE)
greg_report2016.save
greg_report2016.university_services.build(position: 'Facilities Assessment for New Electrical and Computer Engineering Building',
                                            description: 'Conducted a detailed assessement of cost-benefit to the University of a proposed project to construct a new Electrical and Computer Engineering Building on the main campus. Made the recommendation to not proceed with the project due to unforseen cost overruns that the estimator had not provided.')
greg_report2016.university_services.build(position: 'Chair: ECE Department Research Modernization Committee',
                                            description: 'Chaired the committe to revamp the research process within the department, directly leading to an increase of $24 million in secured funding.')
greg_report2016.save
greg_report2016.professional_services.build(description: 'President of ABQ chapter of IEEE Computer Society. Led over 22 members to contribute to 5 technical conferences nationwide. Members raised enough funds to provide a scholarship for women entering STEM degrees in college.',
                                           title: 'President ABQ Chapter IEEE Computer Society.')
greg_report2016.professional_services.build(title: 'NSERC CGS Scholarship Eligibility Voter',
                                            description: 'Member of scholarship eligibility board for the NSERC CGS Scholarship. Graded over 100 submitted technical essays by prospective applicants, providing valuable feedback to the committee chair to make an informed decision. Helped 10 students receive scholarships for graduate degrees.')
greg_report2016.save
greg_report2016.outreach_activities.build(name: 'Computer Camp Mentor and Counselor',
                                          description: 'Camp Counselor and Mentor for the Oakland, CA Computer Camp from July 15-August 10. Provided leadership and mentoring for over 50 camp attendees. Activities include computer programming, website coding and development, app development for mobile devices, and video game design.')
greg_report2016.outreach_activities.build(name: 'Leader: UNM Research Challenge Bootcamp',
                                          description: 'Senior leader of the UNM Research Challenge Bootcamp held on 9/17/2016. Led workshop style training of small groups in the topics of data research, collection, assembly, and final print.')
greg_report2016.save
greg_report2016.grants.build(title: 'U.S. Department of Energy Sunshot Initiative',
                             status: 1,
                             duration: '4 years',
                             total_funding: 10000000,
                             unm_portion: 1500000,
                             soe_portion: 400000,
                             granscol: 'School of Engineering')
greg_report2016.grants.build(title: 'Army Research Office Grant #4002330',
                             status: 1,
                             duration: '1 year',
                             total_funding: 7800000,
                             unm_portion: 4500000,
                             soe_portion: 4100000,
                             granscol: 'School of Engineering')
greg_report2016.save
greg_report2016.mentees.build(mentee_type: 'phd_student',
                              first_name: 'Richie',
                              middle_name: 'Yu',
                              last_name: 'Smith',
                              banner_id: 222233300,
                              study_stage: '60% complete',
                              expected_graduation: may2017,
                              support_source: 'Self Funded',
                              placement: 'unknown')
dec2018 = '2018-12-1'.to_date
greg_report2016.mentees.build(mentee_type: 'masters_student',
                              first_name: 'Sandra',
                              middle_name: 'Ellis',
                              last_name: 'Lee',
                              banner_id: 8889789789,
                              study_stage: '10% complete',
                              expected_graduation: dec2018,
                              support_source: 'Pell Grant',
                              placement: 'unknown')
greg_report2016.save
dfeezel = Person.create( first_name: 'Daniel',
                         last_name: 'Feezell',
                         net_id: 'dfeezell',
                         banner_id: '8883984873',
                         college: engineering,
                         department: ece)
dfeezel.add_role(:user, ece)
dfeezel.save

devet = Person.create(first_name: 'Michael',
                      last_name: 'Devetsikitiotis',
                      net_id: 'mdevets',
                      banner_id: '102938475',
                      college: engineering,
                      department: ece)
devet.add_role(:user, ece)
devet.add_role(:department_admin, ece)
devet.save

devet_report2015 = devet.reports.build(report_year: year2015,
                                       open_time: report_open_time2015,
                                       close_time: report_close_time2015,
                                       rank: 'professor',
                                       department: ece.name,
                                       self_evaluation_text: 'A stupendous year for the Electrical and Computer Engineering Department! We saw the introduction of 5 new courses for graduate students, including Big Data, and Artificial Intelligence. Along with the academic overhaul, we hired 3 new faculty and a new associate professor. Through my leadership, the department secured over $100 million in various grants, and produced over 60 research documents. Additionally, I produced a new textbook for the Introduction to Cloud Computing course ECE 330.',
                                       future_plans_text: 'I would like to continue securing funding sources for the department to further our research fields, especially in the advanced materials area. This would allow the CHTM to continue to innovate and produce better circuits for further use in industry. Additionally I would like to revamp the Operating Systems course ECE 431 to better reflect modern projects that have evolved over the past two decades.') 
devet_report2015.save
devet_report2015.publications.build(publication_type: 'Textbook',
                                    body: 'Devetsikitiotis, M. (2015), "Cloud Computing in the Modern Age". UNM Publishing House',
                                    url: 'http://www.unm.edu/publications/2015/212123.asp')
devet_report2015.publications.build(publication_type: 'Research Paper',
                                    body: 'Devetsikitious, M. & Hanks, T. (2015). Neural Networks for Quality of Service control in Cloud Servers',
                                    url: 'http://www.computers.org/pubs/2015/networking/january/devets_hanks.html')
devet_report2015.save
devet_report2015.courses.build(course_number: 'ECE 350',
                               course_title: 'Modern Processor Design - Intermediate Level',
                               enrollment: 41,
                               year: year2015,
                               semester: 'spring',
                               evaluation_results: 'Monitoring by Engineering Dean was performed on Jan 19. Feedback included positive use of white board during instruction, effective student interactions including asking questions, and useful and informative slides for the powerpoint presentation. Student feedback at the end of the semester was also positive with most students noting they would like to learn more in future classes.',
                               taught: TRUE)
devet_report2015.courses.build(course_number: 'ECE 563',
                               course_title: 'Advanced Computer Processor Design',
                               enrollment: 12,
                               year: year2015,
                               semester: 'spring',
                               evaluation_results: 'Mixed results from the students. One wrote that lectures were ineffective and need to match the textbook more. Another wrote however that lectures expanded upon the topics in the textbook even more and appreciated lectures.',
                               taught: TRUE)
devet_report2015.courses.build(course_number: 'ECE 411',
                               course_title: 'Advanced Computer Mathematical Theory',
                               enrollment: 13,
                               year: year2015,
                               semester: 'fall',
                               evaluation_results: 'Little feedback except one student failed the course and wrote in the course critiques that the Professor was not available at all times for homework help. Office hours were always posted and I was available at all sessions',
                               taught: TRUE)
devet_report2015.courses.build(course_number: 'ECE 541',
                               course_title: 'Circuits in Modern Control Systems',
                               enrollment: 6,
                               year: year2015,
                               semester: 'fall',
                               evaluation_results: 'Positive response from all students. Most stated they would take another course in the field again, and would like to learn more.',
                               taught: TRUE)
devet_report2015.courses.build(course_number: 'ECE 330',
                               course_title: 'Introduction to Cloud Computing',
                               enrollment: 39,
                               year: year2015,
                               semester: 'fall',
                               evaluation_results: 'First semester for this course that included the new texbook that I wrote. Students complained of minor typographical errors, of which will be corrected in the next edition. Otherwise typical feedback from the group.',
                               taught: TRUE)
devet_report2015.courses.build(course_number: 'ECE 412', 
                               course_title: 'Circuits II',
                               enrollment: 21,
                               year: year2015,
                               semester: 'spring',
                               evaluation_results: 'An audited course, to verify the teaching abilities of Mr. Smith, our new faculty instructor. Overall he performed well at his duties, and received good marks from the students.',
                               taught: FALSE)
devet_report2015.save
devet_report2015.professional_services.build(title: 'IEEE 2015 Symposia Presentor',
                                             description: 'Led a symposia at the IEEE Conference in London, UK on June 10. Topics included Optical Networks and Communication as well as Processing for Communications. Over 50 people attended the talks, and received an award at the end of the conference for best presentation in the field of communications.')
devet_report2015.professional_services.build(title: 'EPRI Integrated Risk Technology Users Group Facilitator',
                                             description: 'Organized and led a working group for the Electric Power Research Institute in the Integrated Risk Technology field. Demonstrated functionality of new code to determine risk of power control system automation and modeling probability risk analysis to various applications including hydro dam control, nuclear power control systems, and solar fields.')
devet_report2015.save
devet_report2015.university_services.build(position: '2015 Academic Overhaul Committee Chair',
                                           description: 'Chaired a 4 person committee to overhaul the undergraduate program within the department to include modern operating systems, hardware and coding languages. This committee overhauled 3 existing, and created 5 new courses within the curriculum to facilitate these goals. Additionally 2 courses were scrapped due to obsolence from technology innovations.') 
devet_report2015.university_services.build(position: 'Senior Fellow-UNM Leadership Development Project',
                                           description: 'Continued my work within the UNM Leadership Development Project which is working to develop over 400 faculty and staff members into the leaders of the future. I expect this program will continue to receive the support of the University, due to the number of senior leaders within the University that have gone through the program and developed into excellent leaders.')
devet_report2015.save
devet_report2015.grants.build(title: 'NSF Division of Birational Geometry',
                              status: 2,
                              duration: '4 months',
                              total_funding: 45000,
                              unm_portion: 45000,
                              soe_portion: 45000,
                              granscol: 'School of Engineering')
devet_report2015.grants.build(title: 'SaTC-EDU Big Data and Security-Educating the Next-Generation Security Analysts',
                              status: 1,
                              duration: '1 year 6 months',
                              total_funding: 34000000,
                              unm_portion: 1200000,
                              soe_portion: 1200000,
                              granscol: 'School of Engineering')
devet_report2015.grants.build(title: 'Electrical Circuit Harmonic Analysis Grant',
                              status: 2,
                              duration: '5 months',
                              total_funding: 140000,
                              unm_portion: 100000,
                              soe_portion: 100000,
                              granscol: 'School of Engineering')
devet_report2015.grants.build(title: 'PFI-TT Video Collabatory-Platforms for Viewing and Collecting Electronic Data',
                              status: 1,
                              duration: '2 year',
                              total_funding: 13000000,
                              unm_portion: 4500000,
                              soe_portion: 4500000,
                              granscol: 'School of Engineering')
devet_report2015.save
devet_report2015.outreach_activities.build(name: 'Volunteer - Rio Rancho Basin Bat Project Volunteer',
                                           description: 'Volunteered 150 hours to the Rio Rancho Basin Bat Project, which aims to preserve local area dams and ponds via the use of bats. The main goal is to eliminate mosquitoes and other bugs in standing water, thus keeping deadly diseases such as West Nile Virus away from residential areas.')
devet_report2015.outreach_activities.build(name: 'NMVFO Charity Auction Organizer',
                                           description: 'Organized a charity auction for the New Mexico Volunteers for The Outdoors, raising over $12,000 for the preservation of local trails and outdoor facilities in New Mexico.')
devet_report2015.save
may2020 = '2020-5-1'.to_date
devet_report2015.mentees.build(mentee_type: 'phd_student',
                               first_name: 'Brock',
                               middle_name: 'Harry',
                               last_name: 'Henderson',
                               banner_id: 111999299,
                               study_stage: '1st Year',
                               expected_graduation: may2020,
                               support_source: 'CHTM General Funding',
                               placement: 'unknown')
devet_report2015.mentees.build(mentee_type: 'masters_student',
                               first_name: 'Jerry',
                               middle_name: 'Cole',
                               last_name: 'Ward',
                               banner_id: 999903494,
                               study_stage: '50% complete',
                               expected_graduation: dec2018,
                               support_source: 'Montgomery GI Bill & NASA Aspiring Astronaut Scholarship',
                               placement: 'unknown')
devet_report2015.save

devet_report2016 = devet.reports.build(report_year: year2016,
                                       open_time: report_open_time2016,
                                       close_time: report_close_time2016,
                                       rank: 'professor',
                                       department: ece.name,
                                       self_evaluation_text: 'This year saw the advancement of the Electrical and Engineering department in many areas, including several awards, multiple grants, and a breakthrough in Artificial Intelligence research. My own personal research also resulted in a new process for logic chip constructions, and should be implemented into industrial standard practices within the next five years.',
                                       future_plans_text: 'I plan to continue leading the Department in my position, however I am interested in advancing my leadership education to possibly be considered for a position higher within the University. In addition I will be writing another textbook about modern circuit design.')
devet_report2016.save 
devet_report2016.publications.build(publication_type: 'Case Study',
                                    body: 'Devetsikitious, M. (2016). Raman module prototype development and results',
                                    url: 'http://www.design-concepts.com/work/raman.html')
devet_report2016.publications.build(publication_type: 'Journal Article',
                                    body: 'Devetsikitious, M. & Hareesh, K. (2016). Single Phase SOGI-PLL Based Reference Current Extraction for Three-Phase Four-Wire DSTATCOM. Journal of Electrical Engineering: Volume 16 Edition 2.',
                                    url: 'http://www.jee.ro/covers/editions.php?act=art&art=WE1431511959W5553239775ee3')
devet_report2016.save
devet_report2016.courses.build(course_number: 'ECE 350',
                               course_title: 'Modern Processor Design - Intermediate Level',
                               enrollment: 34,
                               year: year2016,
                               semester: 'spring',
                               evaluation_results: 'A more effective semester this year. Overall performance on exams by students was 8% higher from a year prior. Students provided positive feedback in the end of course critiques, and 95% stated they would recommend the course to fellow pupils.',
                               taught: TRUE)
devet_report2016.courses.build(course_number: 'ECE 560',
                               course_title: 'Modular Electrical Control Circuits',
                               enrollment: 9,
                               year: year2016,
                               semester: 'spring',
                               evaluation_results: 'Good feedback overall, with most students expressing satisfaction with the course.',
                               taught: TRUE)
devet_report2016.courses.build(course_number: 'ECE 411',
                               course_title: 'Advanced Computer Mathematical Theory',
                               enrollment: 14,
                               year: year2016,
                               semester: 'fall',
                               evaluation_results: 'Overall a successful course, however one student did fail. I am uncertain why he was unable to pass the rigors of the course, as I provided a significant amount of personal tutoring and help. I would recommmend he take a more basic course and then reattempt this course another year. Otherwise the students had a successful semester in the class.',
                               taught: TRUE)
devet_report2016.courses.build(course_number: 'ECE 518',
                               course_title: 'Diffraction, Fourier Analysis and Optics',
                               enrollment: 18,
                               year: year2016,
                               semester: 'fall',
                               evaluation_results: 'An excellent group of students that excelled in the topics. I was particularly impressed with the end of year projects, where novel methods of solving wave propagation problems were presented by the students. I would recommend at least 4 of the students to continue their studies to the p.H.D. level.',
                               taught: TRUE)
devet_report2016.save
devet_report2016.professional_services.build(title: 'Engineering Marvels - Television Show Presenter',
                                             description: 'Served as guest speaker for the History Channel television show Engineering Marvels, speaking on the subject of the history of computers and how it revolutionized the world.')
devet_report2016.professional_services.build(title: 'Howard Hughes Doctoral Fellow - Caltech',
                                             description: 'Worked as a Fellow for the Caltech Howard Hughest Doctoral group, studying information theory, communications and radar. Our group discovered an advanced method to send radio signals extremely long distances utilizing the upper atmosphere of Earth.')
devet_report2016.save
devet_report2016.university_services.build(position: 'Session Chair - Photonic and Plasmonic Materials for Enhanced Photovolatic Performance',
                                           description: 'Served as the Session chair for the Photonic and Plasmonic Materials for Enhanced Photovolataic Performance workshop at the MRS fall meeting from November 28-December 2.')
devet_report2016.university_services.build(position: 'UNM Professional Enhancement Committee Member',
                                           description: 'Committee member of the UNM Professional Enhancement Committee, dedicated to enhancing the professionalism of faculty at the University of New Mexico.')
devet_report2016.save
devet_report2016.grants.build(title: 'Modeling Nonlinear Comb Generation with Coupled Mode Theory',
                              duration: '1 year',
                              status: 1,
                              total_funding: 600000,
                              unm_portion: 500000,
                              soe_portion: 500000,
                              granscol: 'School of Engineering')
devet_report2016.grants.build(title: 'Multiphysics Design of of thermophotovolataic panels.',
                              duration: '2 years',
                              status: 1,
                              total_funding: 1400000,
                              unm_portion: 500000,
                              soe_portion: 500000,
                              granscol: 'School of Engineering')
devet_report2016.grants.build(title: 'Characterisation and filter design of high-performance Electrical circuits.',
                              duration: '5 months',
                              status: 1,
                              total_funding: 142000,
                              unm_portion: 142000,
                              soe_portion: 142000,
                              granscol: 'School of Engineering')
devet_report2016.grants.build(title: 'Sparse Matrix Transform Code and Application for Medical Industry',
                              status: 2,
                              total_funding: 5100000,
                              unm_portion: 1230500,
                              soe_portion: 1230500,
                              granscol: 'School of Engineering')
devet_report2016.save
devet_report2016.outreach_activities.build(name: 'Volunteer - Rio Rancho Basin Bat Project Volunteer',
                                           description: 'Volunteered 125 hours to the Rio Rancho Basin Bat Project, which aims to preserve local area dams and ponds via the use of bats. The man goal is to eliminate mosquitoes and other bugs in standing water, thus keeping deadly diseases such as the West Nile Virus away from residential areas.')
devet_report2016.outreach_activities.build(name: 'Homeless coordinator - Bienvenidos Outreach INC. Santa Fe, NM',
                                           description: 'Dedicated 350 hours to the Bienvenidos Outreach group in Santa Fe, NM. Provided over 2500 meals and sleeping quarters for over 500 homeless individuals and their families. Other projects included giving free clothing, house wares, and books to the poor.')
devet_report2016.save
devet_report2016.mentees.build(mentee_type: 'phd_student',
                               first_name: 'Brock',
                               middle_name: 'Harry',
                               last_name: 'Henderson',
                               banner_id: 111999299,
                               study_stage: '2nd Year',
                               expected_graduation: may2020,
                               support_source: 'CHTM General Funding',
                               placement: 'unknown')
devet_report2016.mentees.build(mentee_type: 'masters_student',
                               first_name: 'Jerry',
                               middle_name: 'Cole',
                               last_name: 'Ward',
                               banner_id: 999903494,
                               study_stage: '75% complete',
                               expected_graduation: dec2018,
                               support_source: 'Montgomery GI Bill & NASA Aspiring Astronaut Scholarship',
                               placement: 'unknown')
devet_report2016.save

cecchi = Person.create( first_name: 'Joseph',
                       last_name: 'Cecchi',
                       net_id:  'cecchi',
                       banner_id: '123456789',
                       college: engineering,
                       department: eng_dean)
cecchi.add_role(:user, eng_dean)
cecchi.add_role(:department_admin, engineering)
cecchi.save
cecchi_report2015 = cecchi.reports.build(report_year: year2015,
                                         open_time: report_open_time2015,
                                         close_time: report_close_time2015,
                                         rank: 'distinguished_professor',
                                         department: eng_dean.name,
                                         self_evaluation_text: 'A busy year for the School of Engineering. We hosted the 2015 Rocky Mountain Regional Conference of the American Society of Civil Engineers from April 9-11. I was one of more than 100 engineering deans around the country to sign the American Society for Engineering Education (ASEE) Engineering Deans Council Diversity Initiative letter as part of White House Demo day. We also had over 300 degrees awarded this year, a strong testament to the fact that students are increasingly choosing STEM fields as a career choice. Additionally we had two professors in the School receive awards from the American Nuclear Society, a sign of an increasingly strong Nuclear Engineering Department. Finally I was named associate provost for national laboratory relations, reflecting UNMs confidence in my ability to take on the role of advancing our partnership with the national laboratories.',
                                         future_plans_text: 'I will continue to serve as the Dean of the School of Engineering till at least 2017. I would like to see more progress on the accreditation and remodeling of the Farris Engineering building. My past experience as the dean and in overseeing a large school in times of financial uncertainty made him the logical choice. I look forward to continuing to serve the School of Engineering particularly to help position it for a successful national search for the next dean.')
cecchi_report2015.save
cecchi_report2015.courses.build(course_number: 'ChNE 461',
                                course_title: 'Chemicial Reactor Engineering',
                                enrollment: 20,
                                semester: 'spring',
                                year: year2015,
                                evaluation_results: 'A typical semester of this class, with all students passing with average GPA of 3.2. No unusual comments noted on end of semester evaluations. I believe all students in this class will successfully graduate this year.',
                                taught: TRUE)
cecchi_report2015.courses.build(course_number: 'ChNE 486/586',
                                course_title: 'Statistical Design of Experiments for Semiconductor Manufacturing',
                                enrollment: 7,
                                semester: 'spring',
                                year: year2015,
                                evaluation_results: 'A newly developed course, fewer students signed up than I had hoped for. I believe that the course still requires some curriculum development, thanks to the feedback from the students that attended this semester.',
                                taught: TRUE)
cecchi_report2015.courses.build(course_number: 'ME 461',
                                course_title: 'High Performance Engines',
                                enrollment: 35,
                                semester: 'fall',
                                year: year2015,
                                evaluation_results: 'A highly popular course recently created, the class quickly filled up. The students were extremely motivated and excited about the subject matter. Overwhelming positive feedback was received from the students, and most said they would recommend the course to their friends. The semester was capped off with a special presentation from Honda Motorsports, including rides in a Formula One Race car.',
                                taught: TRUE)
cecchi_report2015.courses.build(course_number: 'ChNe 499/515',
                                course_title: 'Sustainable Energy',
                                enrollment: 23,
                                semester: 'fall',
                                year: year2015,
                                evaluation_results: 'Another newly developed course, the subject matter has become increasingly relevant in todays environment. The students seemed very concerned about the future of energy in the world, and with the effects of climate change. Very positive results from the end of semester evaluations was received, and I intend to continue teaching this class in the future.',
                                taught: TRUE)
cecchi_report2015.save
cecchi_report2015.professional_services.build(title: 'Presenter - Materials Research Society 2015 Spring Meeting',
                                              description: 'Remote Plasma Assisted Atomic Layer Deposition of Ultra-thin Pore-sealing for Self-assembled Porous Low-k materials. Ying-Bang Jiang, George Xomertiakes, Zhu Chen, Darren Dunphy, Jiebin Pang, Eric Branson, Joseph L. Cecchi, and C. Jeffrey Brinker, San Francisco, CA')
cecchi_report2015.professional_services.build(title: 'The Gordon Research Conference on Plasma Processing Science, Tilton, NH. August 2015.',
                                              description: 'Guest Speaker at the Gordon Research Conference on Plasma Processing Science, in Tilton, NH on August 2015.')
cecchi_report2015.professional_services.build(title: 'Consultant - Hyundai Microelectronics, Korea',
                                              description: 'Provided valuable consulting services to the Hyndai Microelectronics corporation in South Korea. Helped to develop a more efficient method to deposit ultra thin Porous Low-k material on microchips, resulting in a 23% increase in manufacturing efficiency.')
cecchi_report2015.save
cecchi_report2015.university_services.build(position: 'Member - Faculty Senate Budget Committee',
                                            description: 'A member of the UNM Faculty Senate Budget Committee, providing necessary budget plans for upcoming academic years at the university.')
cecchi_report2015.university_services.build(position: 'Dean - School of Engineering',
                                            description: 'Acting Dean of the School of Engineering. Manages a large school of thousands of students and hundreds of faculty and staff.')
cecchi_report2015.university_services.build(position: 'Provost',
                                            description: 'Provost at the Masdar Institute of Science and Technology in Masdar City, Abu Dhabi, United Arab Emirates. Additionally, taught two chemical engineering and material science courses remotely to students in Abu Dhabi')
cecchi_report2015.university_services.build(position: 'Chair',
                                            description: 'Chair of the board of directors for the UNM Science and Technology Corporation.')
cecchi_report2015.save
cecchi_report2015.grants.build(title: 'Diagnostic Development for Low-Pressure High-Density Etch Tools - Semiconductor Research Corporation',
                               status: 1,
                               duration: '1 year',
                               total_funding: 100000,
                               unm_portion: 100000,
                               soe_portion: 100000,
                               granscol: 'School of Engineering')
cecchi_report2015.grants.build(title: 'Donation of Advanced Plasma Cluster Tool - Lam Research (Fremont, CA)',
                               status: 1,
                               duration: '3 years',
                               total_funding: 1700000,
                               unm_portion: 1400000,
                               soe_portion: 1400000,
                               granscol: 'School of Engineering')
cecchi_report2015.save
cecchi_report2015.outreach_activities.build(name: 'Member - NM DEDTC Working Group',
                                            description: 'Member of the New Mexico Department of Economic Development Techology Commericalization Working Group. This group allows collaboration between federal and non federal partners within the state in order to optimize resources, share technical expertise and intellectual property, and facilitate rapid commericalization of federally-developed technology.')
cecchi_report2015.outreach_activities.build(name: 'Member - MIT EIG Board',
                                            description: 'Member of the Massachusetts Institute of Technology (MIT) Energy Initiative Governing Board. This board forms a research partnership globally that performs energy research to help meet the worlds need for clean electricity and energy efficiency.')
cecchi_report2015.save
cecchi_report2016 = cecchi.reports.build(report_year: year2016,
                                         open_time: report_open_time2016,
                                         close_time: report_close_time2016,
                                         rank: 'distinguished_professor',
                                         department: eng_dean.name,
                                         self_evaluation_text: 'An exciting year for the School of Engineering with many accomplishments on record. We hosted the U.S. Secretary of the Navy Ray Mabus, who toured the Navy/Marine ROTC building and the UNM Center for Emergin Energy Technologies (CEET) and gave a brief presentation entitled Visionary Leadership - Looking Beyond the the Horizon. The ranking of the UNM School of Engineering improved to #82 in the nation, and improvement from #85 according to U.S. News and World Report. We are especially proud of our Nuclear Engineering Department, which is ranked at #15 in the nation. We saw the hiring of a new Electrical and Computer Engineering chair, Michael Devetsikiotis, who comes to us from North Carolina State University. I also pushed for the School of Engineering and the University to put in a bid to manage the Sandia National Laboratory contract, which will strengthen our impact on research in national security and provide many benefits to the local community and the state of New Mexico. Thanks to efforts by the Provost and President, the School of Engineering is part of a collaborative agreement with Honeywell Federal Manufacturing & Technologies. We saw the Dean position become the first endowed deanship at the University. The School began the search for a new Dean this year as well, with candidates being interviewed already.',
                                         future_plans_text: 'I plan on serving as acting Dean of the School of Engineering until Summer 2017, at which point I will turn over the duties and responsibilities to our new Dean select. In addition to the turnover, I would like to take a short sabbatical to consolidate my research, and then return to my teaching in 2018.')
cecchi_report2016.save
cecchi_report2016.courses.build(course_number: 'ChNe 213',
                                course_title: 'Laboratory Electronics',
                                enrollment: 75,
                                year: year2016,
                                semester: 'spring',
                                evaluation_results: 'Typical feedback from the students, with the usual complaints about lab hours not always being convenient to their schedules. Most students did well with little incidents to report.',
                                taught: TRUE)
cecchi_report2016.courses.build(course_number: 'ChNE 419L',
                                course_title: 'Senior Chemical Engineering Laboratory',
                                enrollment: 13,
                                year: year2016,
                                semester: 'fall',
                                evaluation_results: 'A good semester, with all students passing with excellent marks. Feedback overall was positive. The purchase of new lab equipment, especially the Erlynmeyer flasks was crucial to making this class a success this semester.',
                                taught: TRUE)
cecchi_report2016.courses.build(course_number: 'BME 558',
                                course_title: 'Methods of Analysis in Biomedical Engineering',
                                enrollment: 9,
                                year: year2016,
                                semester: 'fall',
                                evaluation_results: 'Most students excelled except one surprising failure. This particular student was doing well, but botched the final exam completely and did not finish the final project. This student may have personal issues that require resolving. Otherwise the relationship with the students was satisfactory as recorded on the end of semester evaluations.',
                                taught: TRUE)
cecchi_report2016.save
cecchi_report2016.professional_services.build(title: 'Presenter - 2016 Electrochemical Society Meeting',
                                              description: 'Presented the topic Ultra-Thin Conformal Pore-Sealing of Low-K Materials by Plasma-Assisted ALD along with Jeffrey Briner and Ying-Bing Jiang. The forum was the third symposium on Atomic Layer Deposition Applications at the 2016 Electrochemical Society Meeting, Washington, D.C. October 2016.')
cecchi_report2016.professional_services.build(title: 'Consultant - Atmel Corporation',
                                              description: 'Consultant for the Atmel Corporation in San Jose, CA. Through my collaboration with their engineers, we were able to increase the manufacturing efficiency of their microcontrollers by 12%, leading to a savings cost of >$25million per year.')
cecchi_report2016.professional_services.build(title: 'Program Co-Chair',
                                              description: 'Program Co-Chair for the New Mexico American Vaccum Society Annual Symposium, Albuquerque, NM.')
cecchi_report2016.save
cecchi_report2016.university_services.build(position: 'Senior Advisor to the Provost',
                                            description: 'Senior Advisor to the Provost for National Laboratory Relations. Office of the Provost.')
cecchi_report2016.university_services.build(position: 'Dean',
                                            description: 'Dean, School of Engineering. University of New Mexico.')
cecchi_report2016.university_services.build(position: 'Chair',
                                            description: 'Chair, Dean of the College of Arts and Sciences Search Committee')
cecchi_report2016.university_services.build(position: 'Member',
                                            description: 'Member of the Search Committee for the Provost and Executive Vice President for Academic Affairs.')
cecchi_report2016.save
cecchi_report2016.grants.build(title: 'Intel - Donation of Phi 660 Scanning Auger Spectrometer.',
                               status: 1,
                               duration: '1 year',
                               total_funding: 175000,
                               unm_portion: 175000,
                               soe_portion: 175000,
                               granscol: 'School of Engineering')
cecchi_report2016.grants.build(title: 'DARPA (Subcontract from Orincon, San Diego, CA). Prototype Development of Adaptive Process Monitoring for Plasma Etch',
                               status: 1,
                               duration: '3 months',
                               total_funding: 79698,
                               unm_portion: 79698,
                               soe_portion: 79698,
                               granscol: 'School of Engineering')
cecchi_report2016.grants.build(title: 'Lucent Technologies, Bell Labs. Donation of Lucas Labs Cluster Etch Tool.',
                               status: 2,
                               duration: '2 years',
                               total_funding: 800000,
                               unm_portion: 700000,
                               soe_portion: 700000,
                               granscol: 'School of Engineering')
cecchi_report2016.grants.build(title: 'Semiconductor research Corporation - Diagnostic Development for Low-Pressure High-Density Etch Tools.',
                               status: 1,
                               duration: '3 months',
                               total_funding: 100000,
                               unm_portion: 100000,
                               soe_portion: 100000,
                               granscol: 'School of Engineering')
cecchi_report2016.save
cecchi_report2016.outreach_activities.build(name: 'Member MIT EIEC',
                                          description: 'Member of the Massachussetts Institute of Technology (MIT) Energy Inititive Executive Committee. The goal of this partnership is to help develop technologies and solutions that will deliver clean, affordable, and plentiful sources of energy. Our mission is to create low and no-carbon solutions that will efficiently and sustainably meet global energy needs while minimizing environmental impact, dramatically reducing greenhouse gas emissions, and mitigating climate change.')
cecchi_report2016.outreach_activities.build(name: 'Member ABQ COCERWP Council',
                                            description: 'Member of the Greater Albuquerque Chamber of Commerce Energy, Renewables, and Water Planning Council. The Mid-Rio Grande Region is expectged to grow by over 600,000 people in the next 25 years. We cant accommodate that growth with energy and water, unnless we prepare for this future population.')
cecchi_report2016.save
