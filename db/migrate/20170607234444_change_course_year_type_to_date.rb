class ChangeCourseYearTypeToDate < ActiveRecord::Migration[5.0]
  def up
    say_with_time "Changing course year columns to dates" do
      add_column :courses, :temp_year, :date
      Course.reset_column_information
      courses = Course.all
      courses.each do |c|
        c.update_attribute(:temp_year, Date.new(c.year, 01,01))
        c.save!
        say "#{c.course_title} updated.", true
      end
    end
    remove_column :courses, :year
    rename_column :courses, :temp_year, :year
  end
  def down
    say_with_time "Changing course year columns to integers" do
      add_column :courses, :temp_year, :integer, :limit => 4
      Course.reset_column_information
      courses = Course.all
      courses.each do |c|
        c.update_attribute(:temp_year, c.year.year.to_i)
        say "#{c.course_title} update.", true
      end
    end
    remove_column :courses, :year
    rename_column :courses, :temp_year, :year
  end
end
