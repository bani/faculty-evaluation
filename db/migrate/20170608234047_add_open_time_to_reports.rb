class AddOpenTimeToReports < ActiveRecord::Migration[5.0]
  def up
    add_column :reports, :open_time, :datetime
  end
  def down
    remove_column :reports, :open_time
  end
end
