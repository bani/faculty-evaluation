class AddCloseTimeToReports < ActiveRecord::Migration[5.0]
  def up 
    add_column :reports, :close_time, :datetime
  end
  def down
    remove_column :reports, :close_time
  end
end
