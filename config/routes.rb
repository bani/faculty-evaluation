Rails.application.routes.draw do

    resources :people do
    
      collection do
         get 'get_departments', to: "people#get_departments"
      end
        resources :reports do
            member do
              get 'get_report' => 'reports#gen_pdf'
            end
            resources :courses do
            end
        end
    end


    resources :colleges do
      resources :departments
    end
    get '/unauthorized' => 'application#unauthorized'
    root 'welcome#index'

    post '/usersearch/' => 'people#usersearch'
    post '/makeadmin' => 'people#makeadmin'
    post '/revokeadmin' => 'people#revokeadmin'
    get '/reports/new' => 'reports#newopenwindow'
    post '/reports/new' => 'reports#createopenwindow'

	  get '/authorize' => 'application#authorize'
    delete '/logout' => 'application#logout'
end
