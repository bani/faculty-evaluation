class PeopleController < ApplicationController
    
    after_action :verify_authorized, except: :usersearch and :index  

    def index
      authorize :person
      @q = Person.ransack(params[:q])
      @people = @q.result(distinct: true)

    end
    def show
        @person = Person.find(params[:id])
        authorize :person
    end
    def new
        @person = Person.new
        authorize :person
    end
    def edit
        @person = Person.find(params[:id])
        authorize :person
    end
    def create
      @person = Person.new(person_params) 
      @person.department = Department.find(person_params[:department_id])
      @person.add_role(:user, Department.find(person_params[:department_id]))
      authorize :person
      if @person.save
            render json: { success: true, person: @person }
      end
    end

    def update
        @person = Person.find(params[:id])
        authorize :person
    end
    def destroy
        @person = Person.find(params[:id])
        authorize :person
        @person.destroy
        render json: { success: true} 
    end
    def usersearch
      q = params[:query]
      d = Department.find params[:department_id]
      if d.name == "Deans Office"
        c = d.college
        c_id = c.id
        p = Person.where(college_id: c_id).where("lower(last_name) LIKE lower('%#{q}%')")
      else  
        p = Person.where(department_id: params[:department_id]).where("lower(last_name) LIKE lower('%#{q}%')")
      end
       p.each do |p|
        if p.has_role? :department_admin, d
          p.department_admin = true
        end
      end
      render json: p, methods: :department_admin
    end
    def makeadmin
      p = Person.find(params[:person_id])
      d = Department.find(params[:department_id])
      authorize :person 
      if d.name == "Deans Office"
        p.add_role(:department_admin, d.college)
      else
        p.add_role(:department_admin, d)
      end
      render json: { success: true, person: p }
    end
    def revokeadmin
      p = Person.find(params[:person_id])
      d = Department.find(params[:department_id])
      c = d.college
      authorize :person
      p.remove_role(:department_admin, d)
      if d.name == "Deans Office"
        p.remove_role(:department_admin, c)
      end
      p.reload
      
      if (p.has_role? :department_admin, d) || (p.has_role? :department_admin, c)
         render json: { success: false }
      else
         render json: { success: true }
      end
    end

    #used to update department drop-down list when college selection is changed (may no longer be needed)
    def get_departments
       @colleges = College.all
       @college = College.find(params[:college_id])
       @departments = @college.departments   
    end

    private
      def person_params
          params.require(:person).permit(:query, :first_name, :last_name, :net_id, :banner_id, :position, :colleges_id, :college_id, :department_id)
     end
end
