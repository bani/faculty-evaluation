class ReportPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope
    end
  end
  def create?
    true
  end
  def new?
    true
  end
  def edit?
    true
  end
  def admin_create
    true
  end
  def update?
    true
  end
  def destroy?
     user.has_role?(:department_admin, :any)
  end
  def createopenwindow?
    true
  end
  def newopenwindow?
    true
  end
end
