jQuery(function() {
  var departments;
  $('#person_department_id').parent().hide();
  departments = $('#person_department_id').html();
  return $('#person_college_id').change(function() {
     var college, escaped_college, options;
     college = $('#person_college_id :selected').text();
     escaped_college = college.replace(/([ #;&,.+*~\'"!^$[\]()=>|\/@])/g, '\\$1');
     options = $(departments).filter("optgroup[label=" + escaped_college + "]").html();
     if (options) {
        $('#person_department_id').html(options);
        return $('#person_department_id').parent().show();
     } else {
        $('#person_department_id').empty();
        return $('#person_department_id').parent().hide();
     }
  });
});

